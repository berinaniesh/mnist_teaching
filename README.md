# Steps to run

- Create a virtual environment using `python -m venv venv`.
- Activate the virtual environment
  + **Windows:** `.\venv\scripts\activate.ps1` or `.\venv\scripts\activate.bat`
  + **Linux:** `./venv/bin/activate` for bash, or `./venv/bin/activate.fish` for fish shell.
- Install the requirements using `pip install -r requirements.txt`.
- Register the ipython kernel to use in jupyter. `python -m ipykernel install --user --name=mnist_kernel`.
- Start jupyter server using the command `jupyter server` and click on the link shown to open the upyter lab environment. 
- Select the `mnist_kernel` from the dropdown menu at the start. 
- From the kernel menu, select "Restart and run all cells".

